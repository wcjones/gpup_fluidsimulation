//#pragma once

#include "Data/FluidCell.h"
#include "glm/glm.hpp"

#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>

class SimulationThreadPool {
private:
	//DO NOT CHANGE THIS FOR NOW. I am writing the code assuming 8.
	const unsigned int numThreads = 8;
	std::thread *workerPool;

	FluidCell (*cells)[256][256];
	glm::vec4 *cellRenderData;

	std::mutex workerMutex, masterMutex, uniqueMutex;
	std::condition_variable masterVariable, workerVariable;

	std::atomic_int syncCountdown;
	std::unique_lock<std::mutex> masterLock;

	const float timeScale = 0.000001f; // Multiplier for delta time
	const float flowScale = 1.0f;  //Units per second

	//The stage number to process;
	/*
	-2 = End the threads
	-1 = Wait
	0 = Pack Data
	1 = Calculate Averages
	...
	*/
	std::atomic_int processStageNum;

	float deltaTime = 0;
	unsigned int frame = 0;

public:
	SimulationThreadPool(FluidCell(*f)[256][256]);
	void Step(float deltaTime);
	void RunStage(int stage);
	glm::vec4* getRenderDataPointer() { return cellRenderData;  }
	void PackData(glm::vec4*);
	static void SimulationWorkerFunction(SimulationThreadPool* pool, int id);
};
