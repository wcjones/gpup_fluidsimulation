#pragma once
#include "glm/glm.hpp"

struct FluidCell {
	float mass;


	/*
		[0] - X Axis
		[1] - Y Axis
		[2] - Z Axis

		.X  - Positive
		.Y  - Negative	
	
	*/
	glm::vec2 discreteFlow[3];

	/*
	
	0 = Normal;
	1 = Source;
	2 = Sink;
	-1 = Wall;

	*/

	float type;
};
