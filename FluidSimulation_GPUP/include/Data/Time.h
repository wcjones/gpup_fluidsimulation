#pragma once

#include <chrono>
#include <thread>

struct Time {
private:
	float _deltaRenderTime = 0.0f;
	float _deltaCalcTime = 0.0f;
	float _deltaUXTime = 0.1f;

	float _realTime = 1.0f;
	float _scaledTime = 1.0f;
	float _timeScale = 1.0f;

	float _lastRenderTime = 0.0f;
	float _lastCalcTime = 0.0f;
	float _lastUXTime = 0.0f;

	unsigned long long timeFrames = 0;
	unsigned long long renderFrames = 0;
	unsigned long long calcFrames = 0;
	unsigned long long uxFrames = 0;

	float _lastUpdateTime = 0;

	std::chrono::high_resolution_clock::time_point programStart;

public:
	const float &deltaRenderTime = _deltaRenderTime;
	const float &deltaCalcTime = _deltaCalcTime;
	const float &deltaUXTime = _deltaUXTime;

	const float &time = _scaledTime;
	const float &realTime = _realTime;
	const float &timeScale = _timeScale;

	const float targetRenderFPS = 0.01f;  //100 FPS

	//This is how often the simulation data is pushed to the gpu.
	//The actual simulation runs in it's own thread at full speed.
	const float targetCalcFPS   = 0.125f;  
											
	const float targetUXFPS     = 0.001f;  //1000 FPS
	const float targetUpdateDelta    = 0.0005f; //2000 FPS
	const std::chrono::microseconds updateWaitTime = std::chrono::microseconds(100);

	Time() {
		programStart = std::chrono::high_resolution_clock::now();
	}

	void updateTime() {
		_realTime = std::chrono::duration<float>(std::chrono::high_resolution_clock::now() - programStart).count();
		_scaledTime = _realTime * _scaledTime;

		if (_realTime - _lastUpdateTime < targetUpdateDelta)
			std::this_thread::sleep_for(updateWaitTime);

		timeFrames++;
	}
	
	bool tryStepRenderTime() {
		if (realTime - _lastRenderTime > targetRenderFPS) {
			++renderFrames;
			_deltaRenderTime = realTime;
			return true;
		} 
		return false;
	}

	bool tryStepCalcTime() {
		if (realTime - _lastCalcTime > targetCalcFPS) {
			++calcFrames;
			_lastCalcTime = realTime;
			return true;
		}
		return false;
	}

	bool tryStepUXTime() {
		if (realTime - _lastUXTime > targetUXFPS) {
			++uxFrames;
			_lastUXTime = realTime;
			return true;
		}
		return false;
	}

};