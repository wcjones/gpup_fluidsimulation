#pragma once

#include "Graphics.h"
#include "Simulation.h"
#include "UserX.h"
#include "Data.h"

class Engine {
private :
	Data* data;
	Simulation* simulation;
	Graphics* graphics;
	UserX* userx;
public:
	Engine();
	int Initialize();
	int Run();
	int Finalize();
};