#pragma once

#include "Data.h"
#include "GLFW/glfw3.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

class UserX {
private:
	Data* data;
public:
	UserX(Data*);
	int Initialize();
	int Update();
	int Finalize();
};
