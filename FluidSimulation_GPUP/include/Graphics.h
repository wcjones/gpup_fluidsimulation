#pragma once

#include "Data.h"
#include "GL/glew.h"
#include "GLFW/glfw3.h"

class Graphics {
	private:
		Data* data;
		int loadShaders(bool first_pass);
		int renderNormal();
		int renderDebug();
		int renderCam();
	public:
		Graphics(Data*);
		int Initialize();
		int Render();
		int Close();
};
