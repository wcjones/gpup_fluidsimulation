#pragma once

#include "Data/Time.h"
#include "Data/FluidCell.h"
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include "glm/glm.hpp"
#include "glm/gtc/quaternion.hpp"
#include "Data/SimulationThreadPool.h"

#include <string>
#include <thread>

#define WINDOW_SIZE_X 1920
#define WINDOW_SIZE_Y 1080

//#define USE_CPU_SIMULATION

enum APPLICATION_STATUS {
	NORMAL = 0,
	CLOSE = 1
};

enum RENDER_PASS_TYPE {
	REND_NORMAL = 0,
	REND_DEBUG = 1,
	REND_CAM = 2
};

class Data {
	public:

		GLFWwindow* window;

		APPLICATION_STATUS applicationStatus = NORMAL;


		//
		// Do these *really* need to be variables?
		// Are these cast to char?
		// 		- Seth, 1 Dec 2015
		//
		const static unsigned int x_count = 256;
		const static unsigned int y_count = 256;
		const static unsigned int z_count = 256;

		const static unsigned int n_count = x_count * y_count * z_count;

		const static unsigned int x_size = x_count - 1;
		const static unsigned int y_size = y_count - 1;
		const static unsigned int z_size = z_count - 1;
		const static unsigned int n_size = n_count - 1;

		//
		// These should be based on the speeds in Time.h
		// 		- Seth, 1 Dec 2015
		//
		const float angles_per_cycle = 15.0f;
		const float move_speed = 100.0f;

		// Camera data
		// ---

		RENDER_PASS_TYPE renderPass = REND_NORMAL;
		RENDER_PASS_TYPE lastRenderPass = renderPass;

		bool reload_shaders = false;
		bool reloadData = false;

		glm::vec3 position;

		const glm::vec3 lookinVec = glm::vec3(0.0f, 0.0f, -1.0f);
		const glm::vec3 upVec = 	glm::vec3(0.0f, 1.0f, 0.0f);
		const glm::vec3 originVec = glm::vec3(0, 0, 0);

		int window_size_x = WINDOW_SIZE_X;
		int window_size_y = WINDOW_SIZE_Y;

		float radius = 512.0f;	// TODO: #DEFINE this
		float theta = 180.0f;
		float phi = 0.0f;

		Time time;

		//This will be initialized in the simulation engine
		//This is updated by the simulation. Do not change.
		glm::vec4 *clientVertexData = new glm::vec4[n_count];
		unsigned int* clientVertexID = new unsigned int[n_count];

		// Shader data
		// ---

		// Shader names
		GLuint geometryShader;
		GLuint vertexShader;
		GLuint fragmentShader;
		GLuint computeShader;
		GLuint renderProgram;
		GLuint computeProgram;
		GLuint vbo;

		// BFO
		GLuint vertexDataPointer;

		// Camera and Object Transform Matricies
		GLuint uniCameraTransform;
		GLuint uniModelTransform;

		GLuint renderVAO;

		GLint dataAttribute;

};
