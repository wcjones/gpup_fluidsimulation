#pragma once

//#define USE_CPU_SIMULATION

#include "Data.h"
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include "glm/glm.hpp"

class Simulation {
private:

	//This will be initialized in the simulation engine;
	//THIS IS MULTITHREADED. DO NOT TOUCH.
	FluidCell(*cellData)[256][256] = new FluidCell[256][256][256]();
	glm::vec4 *vertexDoubleBuffer;

	Data* data;

	std::atomic_bool endSimulation;
	std::atomic_bool pushData;
	std::atomic_bool dataReady;

	std::mutex updateMutex;
	std::condition_variable updateVariable;


	SimulationThreadPool *threadPool;

	std::thread SimulationMaster;

	std::atomic_ulong simFrame;

public:
	Simulation(Data*);
	int Initialize();
	int Step();
	int Finalize();
	bool SimulationLoop();
	void CPU_Loop();
	int GPU_Step();
	void CPU_WorkFunction();
};
