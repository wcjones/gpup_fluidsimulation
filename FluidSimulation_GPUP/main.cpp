#include "Engine.h"

int main()
{
	Engine* engine = new Engine();
	engine->Initialize();
	engine->Run();
	engine->Finalize();
}
