#version 450 core

uniform mat4 ModelTransform;
uniform mat4 CameraTransform;
uniform vec4 camera_position;

in PointOut_VTX {
	vec4 ptColor;
} VTX_OUT[];

out PointOut_GEO {
  vec4 ptColor_GEO;
  //vec3 ptLightFactor;
};

const vec4 verticle_offsets [17] = {
	0.5f * vec4(-1.0f,  1.0f,  1.0f, 1.0f),
  	0.5f * vec4( 1.0f,  1.0f,  1.0f, 1.0f),
 	0.5f * vec4(-1.0f, -1.0f,  1.0f, 1.0f),
  	0.5f * vec4( 1.0f, -1.0f,  1.0f, 1.0f),
 	0.5f * vec4(-1.0f, -1.0f, -1.0f, 1.0f),
  	0.5f * vec4( 1.0f, -1.0f, -1.0f, 1.0f),
 	0.5f * vec4(-1.0f,  1.0f, -1.0f, 1.0f),
  	0.5f * vec4( 1.0f,  1.0f, -1.0f, 1.0f),
 	0.5f * vec4(-1.0f,  1.0f,  1.0f, 1.0f),
  	0.5f * vec4( 1.0f,  1.0f,  1.0f, 1.0f),
  	0.5f * vec4( 1.0f, -1.0f,  1.0f, 1.0f),
  	0.5f * vec4( 1.0f,  1.0f, -1.0f, 1.0f),
  	0.5f * vec4( 1.0f, -1.0f, -1.0f, 1.0f),
 	0.5f * vec4(-1.0f, -1.0f, -1.0f, 1.0f),
 	0.5f * vec4(-1.0f,  1.0f, -1.0f, 1.0f),
 	0.5f * vec4(-1.0f, -1.0f,  1.0f, 1.0f),
 	0.5f * vec4(-1.0f,  1.0f,  1.0f, 1.0f)
};

layout (points) in;
layout (triangle_strip, max_vertices=17) out; //20

void main() {

	// Indicates air; see vertex shader:
	if (gl_in[0].gl_Position.x == 300) return;

	mat4 ModelViewProjectionMatrix = CameraTransform * ModelTransform;

	// Produce a cube:
	float fancy_factor = 1.0f;
	for (int i = 0; i < 17; i++) {
		gl_Position = ModelViewProjectionMatrix * vec4(gl_in[0].gl_Position + verticle_offsets[i]);
		//fancy_factor = cos(fancy_var) * cos(fancy_var);
		fancy_factor = 20 * (cos((gl_in[0].gl_Position.x * (2 * 3.14159 / 16)) / 256.0f) * sin((gl_in[0].gl_Position.y * (2 * 3.14159 / 16)) / 256.0f))/2 + cos((gl_in[0].gl_Position.z * (2 * 3.14159 / 16)) / 256.0f)/2;
		ptColor_GEO = VTX_OUT[0].ptColor + vec4(0.015f * (i % 2) * fancy_factor, 0.015f * (i % 3) * fancy_factor, 0.015f * (i % 5) * fancy_factor, 0.0f);
		//ptNormal =
		EmitVertex();
	}
	EndPrimitive();

}
