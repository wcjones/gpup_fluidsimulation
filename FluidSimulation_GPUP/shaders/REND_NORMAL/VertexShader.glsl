#version 450 core

// vec3 flow direction with fullness as the additional w float:
in vec4 vertexData;

// Send to geometry shader to create a cube:
out PointOut_VTX {
	vec4 ptColor;
};

void main()
{

	bool air = (vertexData.w == 0);
	bool source = (vertexData.w == -1);
	bool sink = (vertexData.w == -2);
	bool wall = (vertexData.w == -3);
	float fullness = (vertexData.w);

	if (!air) {

		// Here we see FPU engineers are polishing their CVs
		float realZ = -127 + (gl_VertexID % 256);
		float realY = -127 + (int(floor(gl_VertexID / 256)) % 256);
		float realX = -127 + floor(gl_VertexID / (256*256));
		vec4 realPosition = vec4(realX, realY, realZ, 1.0);

		// OLD: apply model-view-projection transform to the vertex:
		// NEW: forward to the geopmetry shader
		gl_Position = realPosition;

		// Blue, with cyan coloring reflecting the turbulence on the horizontal plane.
		//   I predict which cubes are "solid" if they all have identical components (w == 0.8 as well).
		//   That assumption from the simulation.
		// TODO: z-ordering issue prevents me from making a simulation with proper looking alpha blending.
		if (source) {
			ptColor = vec4(0.2f, 1.0f, 0.2f, 1.0f);
		}
		else if (sink) {
			ptColor = vec4(1.0f, 0.2f, 0.2f, 1.0f);
		}
		else if (wall) {
			float lighter = 0.0f;
			if (gl_VertexID % 2 == 0) lighter = 0.2f;
			if (gl_VertexID % 5 == 0) lighter = 0.3f;
			if (gl_VertexID % 10 == 0) lighter = 0.4f;
			ptColor = vec4(0.2f + lighter, 0.2f + lighter, 0.2f + lighter, 1.0f);
		}
		else {
			ptColor = vec4(0.1f, 0.1f, 0.5f + (abs(vertexData.x) + abs(vertexData.y) + abs(vertexData.z)) / 6.0f, fullness / 2.0f + 0.5f);
		}

	} else {
		gl_Position = vec4(300, 0, 0, 1);
		return;
	}

}
