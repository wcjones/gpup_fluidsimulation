#version 450 core

in flat vec3 fColor;

out vec4 outColor;

in PointOut_GEO {
  vec4 ptColor_GEO;
} ptOut;

void main()
{
	outColor = ptOut.ptColor_GEO;
}
