#version 450 core

uniform mat4 ModelTransform;
uniform mat4 CameraTransform;

// vec3 flow direction with fullness as the additional w float:
in vec4 vertexData;

out PointOut_VTX {
	vec4 ptColor;
};

void main()
{

	// Here we see FPU engineers are polishing their CVs
	float realZ = -127 + (gl_VertexID % 256);
	float realY = -127 + (int(floor(gl_VertexID / 256)) % 256);
	float realX = -127 + floor(gl_VertexID / (256*256));
	vec4 realPosition = vec4(realX, realY, realZ, 1.0);

	// apply model-view-projection transform to the vertex:
	gl_Position = CameraTransform * ModelTransform * realPosition;

	// Blue, slightly greener as we approach the top:
	ptColor = vec4(cos(realZ / 256.0f), sin(realY / 256.0f), (sin(realX / 256.0f) + cos(realX / 256.0f)) / 2.0f, 0.5f);

}
