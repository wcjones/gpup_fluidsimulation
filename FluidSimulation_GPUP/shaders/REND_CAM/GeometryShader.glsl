#version 450 core

in PointOut_VTX {
	vec4 ptColor;
} VTX_OUT[];

out PointOut_GEO {
  vec4 ptColor_GEO;
};

layout (points) in;
layout (points, max_vertices=1) out;

void main() {
	gl_Position = gl_in[0].gl_Position;
	ptColor_GEO = VTX_OUT[0].ptColor;
	EmitVertex();
	EndPrimitive();
}
