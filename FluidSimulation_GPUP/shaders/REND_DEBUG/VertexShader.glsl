#version 450 core

uniform mat4 ModelTransform;
uniform mat4 CameraTransform;

// vec3 flow direction with fullness as the additional w float:
in vec4 vertexData;

out PointOut_VTX {
	vec4 ptColor;
};

void main()
{

	// Here we see FPU engineers are polishing their CVs
	float realZ = -127 + (gl_VertexID % 256);
	float realY = -127 + (int(floor(gl_VertexID / 256)) % 256);
	float realX = -127 + floor(gl_VertexID / (256 * 256));
	vec4 realPosition = vec4(realX, realY, realZ, 1.0);

	// apply model-view-projection transform to the vertex:
	gl_Position = CameraTransform * ModelTransform * realPosition;

	// Blue, slightly greener as we approach the top:
	//ptColor = vec4(0.0f, ((realY + 256.0f) / 1024.0f), 0.6f, 0.0f);

	// TODO: !!!

	//There needs to be a better way to do this.
	//Slows the fuck out of the simulation, so distributing the load across GPUs.

	if (vertexData.w == -1) {
		ptColor = vec4(0.5, 0.5, 1, 0.8);
	}
	else if (vertexData.w == -2) {
		ptColor = vec4(1, 0.5, 0.5, 0.8);
	}
	else if (vertexData.w == -3) {
		ptColor = vec4(0.5, 0.5, 0.5, 0.8);
	}
	else {
		ptColor = vertexData;
	}

	//ptColor = vertexData;

}
