#include "Simulation.h"
#include <thread>
#include <condition_variable>
#include <mutex>
#include <atomic>
#include <iostream>
#include <chrono>

using namespace std;

void Simulation::CPU_Loop() {

	unique_lock<mutex> updateLock(updateMutex, defer_lock);
	chrono::high_resolution_clock::time_point lastFrame = chrono::high_resolution_clock::now();
	chrono::high_resolution_clock::time_point thisFrame;

	do {
		thisFrame = chrono::high_resolution_clock::now();

		threadPool->Step((thisFrame - lastFrame).count() / 1000000.0f);

		lastFrame = thisFrame;

		//Check if new data should be pushed.

		if (pushData.load() == true) {
			updateLock.lock();
			threadPool->PackData(vertexDoubleBuffer);
			pushData.store(false);
			dataReady.store(true);
			updateLock.unlock();
		}

		simFrame.fetch_add(1);

	} while (!endSimulation.load());

	return;
}
