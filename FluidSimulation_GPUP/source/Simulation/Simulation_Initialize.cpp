#include "Simulation.h"

#include <chrono>
#include <cmath>
#include <iostream>

using namespace std;

void RunSimulationMaster(Simulation* s) { s->SimulationLoop(); }

int Simulation::Initialize() {

	//dataReady = atomic_bool();
	dataReady.store(false);

	//pushData = atomic_bool();
	pushData.store(false);

	endSimulation.store(false);

	threadPool = new SimulationThreadPool(cellData);

	// Prevent a race condition in the constructor for SimulationThreadPool.
	//   It spawns threads which immediately aquire a lock. If we RunStage or
	//   run the simulation loop, we risk getting caught in the race condition.
	//   This is a bandaid. -S
	std::this_thread::sleep_for(std::chrono::nanoseconds(1000));

	//Tell the thread pool to initialize.
	threadPool->RunStage(-3);

	//Tell the thread pool to pack.
	threadPool->PackData(data->clientVertexData);

	vertexDoubleBuffer = new glm::vec4[data->n_count];
	threadPool->PackData(vertexDoubleBuffer);



	glm::vec4 &check = data->clientVertexData[526344];
	cout << "Check: " << check.x << ", " << check.y << ", " << check.z << ", " << check.w << endl;

	SimulationMaster = thread(RunSimulationMaster, this);
	SimulationMaster.detach();


	return 0;
}
