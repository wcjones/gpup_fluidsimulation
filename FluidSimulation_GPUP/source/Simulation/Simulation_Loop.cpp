#include "Simulation.h"

using namespace std;

bool Simulation::SimulationLoop() {

#ifdef USE_CPU_SIMULATION
	CPU_Loop();
#else
	GPU_Step();
#endif

	return !endSimulation.load();
}