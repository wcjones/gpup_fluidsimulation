#include "Data/SimulationThreadPool.h"

using namespace std;

SimulationThreadPool::SimulationThreadPool(FluidCell(*f)[256][256]) :
workerPool(new thread[numThreads]),
workerMutex(), masterMutex(), uniqueMutex(),
masterVariable(), workerVariable(),
syncCountdown(),
masterLock(masterMutex, defer_lock),
processStageNum()
{
	cells = f;

	processStageNum.store(0);
	syncCountdown.store(0);

	for (unsigned int i = 0; i < numThreads; i++) {
		workerPool[i] = thread(SimulationWorkerFunction, this, i);
		workerPool[i].detach();
	}
}
