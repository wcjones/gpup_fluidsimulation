#include "Data/SimulationThreadPool.h"

#include <iostream>
#include <chrono>
#include <ctime>

void SimulationThreadPool::SimulationWorkerFunction(SimulationThreadPool *pool, int id) {

	int stageNum = 0;
	srand((unsigned int)std::time(nullptr) + (unsigned int)(100 + id));
	std::unique_lock<std::mutex> pLock(pool->workerMutex, std::defer_lock);
	std::unique_lock<std::mutex> soloLock(pool->uniqueMutex, std::defer_lock);

	do {

		do {
			pLock.lock();
			pool->workerVariable.wait(pLock);
			pLock.unlock();
		} while ((stageNum = pool->processStageNum.load()) == -1);



		if (stageNum == -2) {
			//Leave the primary loop and end the thread pool.
			break;
		}

		//This should be broken into seperate functions

		switch (stageNum) {
			case -3:
			{
				//Initialize the values
				for (int z = id * 32; z < (id + 1) * 32; z++) {
					for (int y = 0; y < 256; y++) {
						for (int x = 0; x < 256; x++) {

							FluidCell& cell = pool->cells[x][y][z];
							cell.mass = 0;
							cell.type = 0;

							if (x % 8 == 4 && z % 8 == 4 && y == 225) {
								cell.type = -1;
								cell.mass = 0;
							}

							if (y > 150 && y < 205 && x > 96 && x < 160 && z > 96 && z < 160) {
								cell.mass = 1;
							}


							if (y == 50 && x > 32 && x < 224 && z > 32 && z < 224) {
								cell.type = -3;
								cell.mass = 0;
							}
							else if (y >= 50 && y < 100) {
								if ((x > 32 && x < 224) && (z == 32 || z == 224)) {
									cell.type = -3;
									cell.mass = 0;

								}
								else if ((x == 32 || x == 224) && (z > 32 && z < 224)) {
									cell.type = -3;
									cell.mass = 0;
								}
							}

							if (y == 125) {
								if ((x > 96 && x < 160) && (z > 64 && z < 192)) {
									cell.type = -3;
									cell.mass = 0;

								}
								else if ((x > 64 && x < 192) && (z > 96 && z < 160)) {
									cell.type = -3;
									cell.mass = 0;
								}

								if (x > 124 && x < 132 && z > 124 && z < 132)
								{
									cell.type = 0;
								}
							}

							if (y == 0) {
								cell.type = -2;
								cell.mass = 0;
							}

							//For debugging
							//soloLock.lock();
							//cout << "T: " << id << " P: <" << x << ',' << y << ',' << z << "> F: " << cell.mass << endl;
							//soloLock.unlock();
						}
					}
				}
			} break;

			case -2:
			{
				//This should really never happen;
				assert(true);
			} break;

			case -1:
			{
				//This should never happen.
				assert(true);

			} break;

			case 0:
			{
				for (int z = id * 32; z < (id + 1) * 32; z++) {
					for (int y = 0; y < 256; y++) {
						for (int x = 0; x < 256; x++) {
							FluidCell& cell = pool->cells[x][y][z];
							glm::vec4& data = pool->cellRenderData[x + y * 256 + z * 256 * 256];


							if (cell.type == -3) {
								data.x = 0.5f;
								data.y = 0.5f;
								data.z = 0.5f;
								data.a = 0.8f;
							}
							else if (cell.type == -1) {
								data.b = 1.0f;
								data.r = 0.5f;
								data.g = 0.5f;
								data.a = 1.0f;
							}
							else if (cell.type == -2) {
								data.b = 0.5f;
								data.r = 1.0f;
								data.g = 0.5f;
								data.a = 1.0f;
							}
							else {
								data.x = (cell.discreteFlow[0].x*cell.discreteFlow[0].x + cell.discreteFlow[0].y*cell.discreteFlow[0].y) / 1.4142135f / 2;
								data.y = (cell.discreteFlow[1].x*cell.discreteFlow[1].x + cell.discreteFlow[1].y*cell.discreteFlow[1].y) / 1.4142135f / 2;
								data.z = (cell.discreteFlow[2].x*cell.discreteFlow[2].x + cell.discreteFlow[2].y*cell.discreteFlow[2].y) / 1.4142135f / 2;

								if (cell.mass != 0) {
									data.w = cell.mass + 0.75f;
									//data.r = 0.2f;
									//data.g = 0.2f;
									//data.b = 1.0f;
								}
								else {
									data.w = 0;
								}
								//data.w = cell.mass;
							}

							//ioLock.lock();
							//glm::vec4 &check = pool->cellRenderData[x + y * 256 + z * 256 * 256];
							//cout << "Pushing cell: " << check.x << ", " << check.y << ", " << check.z << ", " << check.w << endl;
							//ioLock.unlock();
						}
					}
				}
			} break;

			case 1:
			{
				//Initialize
				for (int z = id * 32; z < (id + 1) * 32; z++) {
					//float layerPressure = 1.0f + 0.0005f * z;
					float layerPressure = 1.0f;
					for (int y = 0; y < 256; y++) {
						for (int x = 0; x < 256; x++) {
							FluidCell &cell = pool->cells[x][y][z];

							cell.discreteFlow[0].x = 0;
							cell.discreteFlow[0].y = 0;
							cell.discreteFlow[1].x = 0;
							cell.discreteFlow[1].y = 0;
							cell.discreteFlow[2].x = 0;
							cell.discreteFlow[2].y = 0;

							if (cell.type == -1) {
								if (rand() % 100 == 0)
									cell.mass = 1;
							}
							else if (cell.type == -2)
								cell.mass = 0;

							if (cell.mass == 0)
								continue;

							//Calculate Downward Mass Flow
							if (y != 0 && !(pool->cells[x][y - 1][z].type == -3)) {

								FluidCell &GS = pool->cells[x][y - 1][z];

								if (GS.mass > 1.05f)
									continue;

								float dMass = 1.05f - GS.mass;

								if (dMass > cell.mass)
									dMass = cell.mass;

								//dMass *= pool->deltaTime;

								cell.mass -= dMass;

								//cell.discreteFlow[1].y -= dMass;
								pool->cells[x][y - 1][z].discreteFlow[1].x += dMass;
							}

						}
					}
				}
			} break;

			case 2:
			{
				for (int z = id * 32; z < (id + 1) * 32; z++) {
					//float layerPressure = 1.0f + 0.0005f * z;
					float layerPressure = 1.0f;
					for (int y = 0; y < 256; y++) {
						for (int x = 0; x < 256; x++) {
							FluidCell &cell = pool->cells[x][y][z];
							//Register Downward Mass Flow
							if (cell.mass < 0)
								cell.mass = 0;

							//Abort if I have nothing to contribute
							if (cell.mass == 0)
								continue;

							float tAvg = 0, nSum = 0, minMass = cell.mass;
							int tNum = 0, nNum = 0;

							tAvg += cell.mass;
							tNum++;

							if (x != 0 && !(pool->cells[x - 1][y][z].type == -3)) {
								tAvg += pool->cells[x - 1][y][z].mass;
								tNum++;

								if (pool->cells[x - 1][y][z].mass < cell.mass) {
									nSum += pool->cells[x - 1][y][z].mass;
									nNum++;
								}

								if (pool->cells[x - 1][y][z].mass < minMass) {
									minMass = pool->cells[x - 1][y][z].mass;
								}

							}

							if (x != 255 && !(pool->cells[x + 1][y][z].type == -3)) {
								tAvg += pool->cells[x + 1][y][z].mass;
								tNum++;

								if (pool->cells[x + 1][y][z].mass < cell.mass) {
									nSum += pool->cells[x + 1][y][z].mass;
									nNum++;
								}

								if (pool->cells[x + 1][y][z].mass < minMass) {
									minMass = pool->cells[x + 1][y][z].mass;
								}
							}

							if (z != 0 && !(pool->cells[x][y][z - 1].type == -3)) {
								tAvg += pool->cells[x][y][z - 1].mass;
								tNum++;

								if (pool->cells[x][y][z - 1].mass < cell.mass) {
									nSum += pool->cells[x][y][z - 1].mass;
									nNum++;
								}

								if (pool->cells[x][y][z - 1].mass < minMass) {
									minMass = pool->cells[x][y][z - 1].mass;
								}
							}

							if (z != 255 && !(pool->cells[x][y][z + 1].type == -3)) {
								tAvg += pool->cells[x][y][z + 1].mass;
								tNum++;

								if (pool->cells[x][y][z + 1].mass < cell.mass) {
									nSum += pool->cells[x][y][z + 1].mass;
									nNum++;
								}

								if (pool->cells[x][y][z + 1].mass < minMass) {
									minMass = pool->cells[x][y][z + 1].mass;
								}
							}

							tAvg /= tNum;
							nSum -= cell.mass * nNum;
							nSum *= -1;

							if (cell.mass <= minMass)
								continue;

							float dMass = 0;

							if (x != 0 && !(pool->cells[x - 1][y][z].type == -3)) {
								dMass = pool->cells[x - 1][y][z].mass;
								if (dMass < cell.mass) {
									dMass = (cell.mass + dMass) * (cell.mass - dMass) / (2 * nSum);

									//dMass *= pool->deltaTime;

									pool->cells[x - 1][y][z].discreteFlow[0].x += dMass;
									cell.discreteFlow[0].y -= dMass;
								}

							}

							if (x != 255 && !(pool->cells[x + 1][y][z].type == -3)) {
								dMass = pool->cells[x + 1][y][z].mass;
								if (dMass < cell.mass) {
									dMass = (cell.mass + dMass) * (cell.mass - dMass) / (2 * nSum);

									//dMass *= pool->deltaTime;

									pool->cells[x + 1][y][z].discreteFlow[0].y += dMass;
									cell.discreteFlow[0].x -= dMass;
								}
							}

							if (z != 0 && !(pool->cells[x][y][z - 1].type == -3)) {
								dMass = pool->cells[x][y][z - 1].mass;
								if (dMass < cell.mass) {
									dMass = (cell.mass + dMass) * (cell.mass - dMass) / (2 * nSum);

									//dMass *= pool->deltaTime;

									pool->cells[x][y][z - 1].discreteFlow[2].x += dMass;
									cell.discreteFlow[2].y -= dMass;
								}
							}

							if (z != 255 && !(pool->cells[x][y][z + 1].type == -3)) {
								dMass = pool->cells[x][y][z + 1].mass;
								if (dMass < cell.mass) {
									dMass = (cell.mass + dMass) * (cell.mass - dMass) / (2 * nSum);

									//dMass *= pool->deltaTime;

									pool->cells[x][y][z + 1].discreteFlow[2].y += dMass;
									cell.discreteFlow[2].x -= dMass;
								}
							}
						}
					}
				}
			} break;

			case 3:
			{
				for (int z = id * 32; z < (id + 1) * 32; z++) {
					for (int y = 0; y < 256; y++) {
						for (int x = 0; x < 256; x++) {
							//Register horizontal flow
							FluidCell & cell = pool->cells[x][y][z];
							cell.mass += cell.discreteFlow[0].x + cell.discreteFlow[0].y;
							cell.mass += cell.discreteFlow[1].x + cell.discreteFlow[1].y;
							cell.mass  += cell.discreteFlow[2].x + cell.discreteFlow[2].y;
							if (cell.mass < 0)
								cell.mass = 0;
						}
					}
				}
			} break;

			case 4:
			{
				//Finalize
				for (int z = id * 32; z < (id + 1) * 32; z++) {
					for (int y = 0; y < 255; y++) {
						for (int x = 0; x < 256; x++) {
							FluidCell& cell = pool->cells[x][y][z];

						}
					}
				}
			} break;

			case 5:
			{
				//Perform Changes
				for (int z = id * 32; z < (id + 1) * 32; z++) {
					for (int y = 0; y < 256; y++) {
						for (int x = 0; x < 256; x++) {
							FluidCell& cell = pool->cells[x][y][z];
						}
					}
				}

			} break;

			default:
				break;
		}

		if (pool->syncCountdown.fetch_sub(1) == 1) {
			pool->masterVariable.notify_all();
		}

	} while(true);
}
