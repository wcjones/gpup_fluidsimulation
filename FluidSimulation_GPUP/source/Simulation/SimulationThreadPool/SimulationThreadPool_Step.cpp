#include "Data/SimulationThreadPool.h"
#include <iostream>

using namespace std;

void SimulationThreadPool::RunStage(int stage) {
	cout << "*** Running stage: " << stage << endl;
	//Setup the atomic notifications.
	processStageNum.store(stage);
	syncCountdown.store(8);

	//cout << "*** Notifying" << endl;
	//Notify the threads of work.
	workerVariable.notify_all();

	//cout << "*** Waiting" << endl;
	//Wait for the threads to finish.
	masterLock.lock();
	masterVariable.wait(masterLock);
	masterLock.unlock();

	//cout << "*** Stage Complete: " << endl;
	//Cleanup
	processStageNum.store(-1);

	//Yield has to occur or bad things.
	this_thread::yield();
}

void SimulationThreadPool::Step(float dt) {
	frame++;
	deltaTime = dt * timeScale;

	//cout << "*** DT: " << deltaTime << endl;

	RunStage(1);
	RunStage(2);
	RunStage(3);
	//RunStage(4);
}

void SimulationThreadPool::PackData(glm::vec4 *d) {
	cellRenderData = d;
	RunStage(0);
}
