#include "Simulation.h"
#include <iostream> 

using namespace std;

int Simulation::Step() {


	if (dataReady.load() == true) {

		unique_lock<mutex> updateLock(updateMutex);
		swap(vertexDoubleBuffer, data->clientVertexData);
		dataReady.store(false);
		updateLock.unlock();

		data->reloadData = true;
	}
	else {
		pushData.store(true);
	}

	//cout << "SSS - Update F:" << simFrame.load() << endl;

	return 0;
}