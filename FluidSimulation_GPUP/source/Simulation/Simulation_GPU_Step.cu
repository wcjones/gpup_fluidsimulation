#include "./CUDA_Kernals/CUDA_Kernals.h"

#include <thread>
#include <condition_variable>
#include <mutex>
#include <atomic>
#include <iostream>
#include <chrono>

#include <stdio.h>

using namespace std;

int Simulation::GPU_Step()
{

	int returnStatus = 0;
	cudaError_t cudaStatus;
	unique_lock<mutex> updateLock(updateMutex, defer_lock);
	CUDA_FluidCell *DeviceCellData1 = 0, *DeviceCellData2 = 0;
	glm::vec4* DeviceRenderData = 0;

	cudaStatus = cudaSetDevice(1);
	if (cudaStatus != cudaSuccess) {
		fprintf(stdout, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		returnStatus = 1;
		goto ErrorState;
	}
	cout << "Cuda Device Set\nFluid Cell Size: " << sizeof(CUDA_FluidCell) << "\nVec4 Size:" << sizeof(glm::vec4) << endl;

	cudaStatus = cudaMalloc((void**)&DeviceCellData1, 256 * 256 * 256 * sizeof(CUDA_FluidCell));
	if (cudaStatus != cudaSuccess) {
		fprintf(stdout, "cudaMalloc failed! - Fluid Cells 1\n");
		goto ErrorState;
	}

	cudaStatus = cudaMalloc((void**)&DeviceCellData2, 256 * 256 * 256 * sizeof(CUDA_FluidCell));
	if (cudaStatus != cudaSuccess) {
		fprintf(stdout, "cudaMalloc failed! - Fluid Cells 2\n");
		goto ErrorState;
	}

	cout << "Loading cells... (May take a few seconds)" << endl;


	//Have to use malloc to avod the constructor.
	CUDA_FluidCell *CellInitData = (CUDA_FluidCell*)malloc(256 * 256 * 256 * sizeof(CUDA_FluidCell));
	for (int x = 0; x < 256; x++) {
		for (int y = 0; y < 256; y++) {
			for (int z = 0; z < 256; z++) {
				CUDA_FluidCell &C = CellInitData[x + y * 256 + z * 256 * 256];
				FluidCell &F = cellData[x][y][z];

				C = CUDA_FluidCell(F);
			}
		}
	}


	cudaMemcpy(DeviceCellData1, CellInitData, 256 * 256 * 256 * sizeof(CUDA_FluidCell), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stdout, "cudaMemCpy failed! - Fluid Cells 1\n");
		goto ErrorState;
	}


	cudaMemcpy(DeviceCellData2, CellInitData, 256 * 256 * 256 * sizeof(CUDA_FluidCell), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stdout, "cudaMemCpy failed! - Fluid Cells 2\n");
		goto ErrorState;
	}

	cudaStatus = cudaMalloc((void**)&DeviceRenderData, 256 * 256 * 256 * sizeof(glm::vec4));
	if (cudaStatus != cudaSuccess) {
		fprintf(stdout, "cudaMalloc failed! - Render Data\n");
		goto ErrorState;
	}

	cudaMemcpy(DeviceRenderData, vertexDoubleBuffer, 256 * 256 * 256 * sizeof(glm::vec4), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stdout, "cudaMemCpy failed! - Render Data\n");
		goto ErrorState;
	}


	fprintf(stdout, "Cuda Global Memory Initialization Success!!!\n");

	int CalcFrame = 0;

	while (!endSimulation.load()) {

		FlowCalculation << <dim3(16, 256, 16), dim3(16, 1, 16), 18*3*18*sizeof(CUDA_FluidCell) >> >(DeviceCellData1, DeviceCellData2, 0.1f, 1.0f);

		cudaStatus = cudaDeviceSynchronize();
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching FlowCalculation!\n", cudaStatus);
			goto ErrorState;
		}

		//cout << "* Flow Calculation\n";

		FlowUpdate << <dim3(16, 256, 16), dim3(16, 1, 16), 18 * 3 * 18 * sizeof(CUDA_FluidCell) >> > (DeviceCellData1, DeviceCellData2);

		cudaStatus = cudaDeviceSynchronize();
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching FlowUpdate!\n", cudaStatus);
			goto ErrorState;
		}

		//cout << "* Flow Update\n";

		//cout << "Cuda Simulation Step Complete. Frame: " << CalcFrame++ << endl;

		if (pushData.load() == true) {
			CellRenderDataPush << <dim3(16, 256, 16), dim3(16, 1, 16), 0 >> > (DeviceCellData2, DeviceRenderData);

			cudaStatus = cudaDeviceSynchronize();
			if (cudaStatus != cudaSuccess) {
				fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching CellRenderDataPush!\n", cudaStatus);
				goto ErrorState;
			}

			updateLock.lock();

			cudaMemcpy(vertexDoubleBuffer, DeviceRenderData, 256 * 256 * 256 * sizeof(glm::vec4), cudaMemcpyDeviceToHost);

			pushData.store(false);
			dataReady.store(true);
			updateLock.unlock();

			//cout << "Device->Host Render Update. \n";
		}

		swap(DeviceCellData1, DeviceCellData2);
	}

ErrorState:
	cudaFree(DeviceCellData1);
	cudaFree(DeviceCellData2);
	cudaFree(DeviceRenderData);
	assert(returnStatus != 0);
	return returnStatus;
}