#include "CUDA_Kernals.h"

using namespace std; 

__global__ void FlowUpdate(CUDA_FluidCell *PreviousCells, CUDA_FluidCell *NextCells) {
	ThreadIndex tid;

	__shared__ CUDA_FluidCell localCells[18][3][18];


	localCells[threadIdx.x + 1][1][threadIdx.z + 1] = NextCells[tid.i];


	//Load the data.
	if (blockIdx.y != 0 && blockIdx.y != 255) {
		//Load Current Position
		localCells[threadIdx.x + 1][0][threadIdx.z + 1] = NextCells[tid.i - 256];
		localCells[threadIdx.x + 1][2][threadIdx.z + 1] = NextCells[tid.i + 256];

		//X Axis
		if (threadIdx.x == 0) {
			if (blockIdx.x != 0 && blockIdx.x != 15) {
				localCells[0][1][threadIdx.z + 1] = NextCells[tid.i - 1];
				localCells[0][0][threadIdx.z + 1] = NextCells[tid.i - 256 - 1];
				localCells[0][2][threadIdx.z + 1] = NextCells[tid.i + 256 - 1];

				localCells[17][1][threadIdx.z + 1] = NextCells[tid.i + 16];
				localCells[17][0][threadIdx.z + 1] = NextCells[tid.i - 256 + 16];
				localCells[17][2][threadIdx.z + 1] = NextCells[tid.i + 256 + 16];

			}
			else if (blockIdx.x == 0) {
				localCells[17][1][threadIdx.z + 1] = NextCells[tid.i + 16];
				localCells[17][0][threadIdx.z + 1] = NextCells[tid.i - 256 + 16];
				localCells[17][2][threadIdx.z + 1] = NextCells[tid.i + 256 + 16];
			}
			else if (blockIdx.x == 15) {
				localCells[0][1][threadIdx.z + 1] = NextCells[tid.i - 1];
				localCells[0][0][threadIdx.z + 1] = NextCells[tid.i - 256 - 1];
				localCells[0][2][threadIdx.z + 1] = NextCells[tid.i + 256 - 1];
			}
		}

		//Z Axis
		if (threadIdx.z == 0) {
			if (blockIdx.z != 0 && blockIdx.z != 15) {
				localCells[threadIdx.x + 1][1][0] = NextCells[tid.i - 65536];
				localCells[threadIdx.x + 1][0][0] = NextCells[tid.i - 256 - 65536];
				localCells[threadIdx.x + 1][2][0] = NextCells[tid.i + 256 - 65536];

				localCells[threadIdx.x + 1][1][17] = NextCells[tid.i + 65536 * 16];
				localCells[threadIdx.x + 1][0][17] = NextCells[tid.i - 256 + 65536 * 16];
				localCells[threadIdx.x + 1][2][17] = NextCells[tid.i + 256 + 65536 * 16];
			}
			else if (blockIdx.z == 0) {
				localCells[threadIdx.x + 1][1][17] = NextCells[tid.i + 65536 * 16];
				localCells[threadIdx.x + 1][0][17] = NextCells[tid.i - 256 + 65536 * 16];
				localCells[threadIdx.x + 1][2][17] = NextCells[tid.i + 256 + 65536 * 16];
			}
			else if (blockIdx.z == 15) {
				localCells[threadIdx.x + 1][1][0] = NextCells[tid.i - 65536];
				localCells[threadIdx.x + 1][0][0] = NextCells[tid.i - 256 - 65536];
				localCells[threadIdx.x + 1][2][0] = NextCells[tid.i + 256 - 65536];
			}
		}

	}
	else if (blockIdx.y == 0) {
		//Load Current Position
		localCells[threadIdx.x + 1][2][threadIdx.z + 1] = NextCells[tid.i + 256];

		//X Axis
		if (threadIdx.x == 0) {
			if (blockIdx.x != 0 && blockIdx.x != 15) {
				localCells[0][1][threadIdx.z + 1] = NextCells[tid.i - 1];
				localCells[0][2][threadIdx.z + 1] = NextCells[tid.i + 256 - 1];

				localCells[17][1][threadIdx.z + 1] = NextCells[tid.i + 16];
				localCells[17][2][threadIdx.z + 1] = NextCells[tid.i + 256 + 16];

			}
			else if (blockIdx.x == 0) {
				localCells[17][1][threadIdx.z + 1] = NextCells[tid.i + 16];
				localCells[17][2][threadIdx.z + 1] = NextCells[tid.i + 256 + 16];
			}
			else if (blockIdx.x == 15) {
				localCells[0][1][threadIdx.z + 1] = NextCells[tid.i - 1];
				localCells[0][2][threadIdx.z + 1] = NextCells[tid.i + 256 - 1];
			}
		}

		//Z Axis
		if (threadIdx.z == 0) {
			if (blockIdx.z != 0 && blockIdx.z != 15) {
				localCells[threadIdx.x + 1][1][0] = NextCells[tid.i - 65536];
				localCells[threadIdx.x + 1][2][0] = NextCells[tid.i + 256 - 65536];

				localCells[threadIdx.x + 1][1][17] = NextCells[tid.i + 65536 * 16];
				localCells[threadIdx.x + 1][2][17] = NextCells[tid.i + 256 + 65536 * 16];
			}
			else if (blockIdx.z == 0) {
				localCells[threadIdx.x + 1][1][17] = NextCells[tid.i + 65536 * 16];
				localCells[threadIdx.x + 1][2][17] = NextCells[tid.i + 256 + 65536 * 16];
			}
			else if (blockIdx.z == 15) {
				localCells[threadIdx.x + 1][1][0] = NextCells[tid.i - 65536];
				localCells[threadIdx.x + 1][2][0] = NextCells[tid.i + 256 - 65536];
			}
		}
	}
	else if (blockIdx.y == 255) {
		//Load Current Position
		localCells[threadIdx.x + 1][0][threadIdx.z + 1] = NextCells[tid.i - 256];

		//X Axis
		if (threadIdx.x == 0) {
			if (blockIdx.x != 0 && blockIdx.x != 15) {
				localCells[0][1][threadIdx.z + 1] = NextCells[tid.i - 1];
				localCells[0][0][threadIdx.z + 1] = NextCells[tid.i - 256 - 1];

				localCells[17][1][threadIdx.z + 1] = NextCells[tid.i + 16];
				localCells[17][0][threadIdx.z + 1] = NextCells[tid.i - 256 + 16];

			}
			else if (blockIdx.x == 0) {
				localCells[17][1][threadIdx.z + 1] = NextCells[tid.i + 16];
				localCells[17][0][threadIdx.z + 1] = NextCells[tid.i - 256 + 16];
			}
			else if (blockIdx.x == 15) {
				localCells[0][1][threadIdx.z + 1] = NextCells[tid.i - 1];
				localCells[0][0][threadIdx.z + 1] = NextCells[tid.i - 256 - 1];
			}
		}

		//Z Axis
		if (threadIdx.z == 0) {
			if (blockIdx.z != 0 && blockIdx.z != 15) {
				localCells[threadIdx.x + 1][1][0] = NextCells[tid.i - 65536];
				localCells[threadIdx.x + 1][0][0] = NextCells[tid.i - 256 - 65536];

				localCells[threadIdx.x + 1][1][17] = NextCells[tid.i + 65536 * 16];
				localCells[threadIdx.x + 1][0][17] = NextCells[tid.i - 256 + 65536 * 16];
			}
			else if (blockIdx.z == 0) {
				localCells[threadIdx.x + 1][1][17] = NextCells[tid.i + 65536 * 16];
				localCells[threadIdx.x + 1][0][17] = NextCells[tid.i - 256 + 65536 * 16];
			}
			else if (blockIdx.z == 15) {
				localCells[threadIdx.x + 1][1][0] = NextCells[tid.i - 65536];
				localCells[threadIdx.x + 1][0][0] = NextCells[tid.i - 256 - 65536];
			}
		}
	}

	__syncthreads();

	CUDA_FluidCell MyCell = localCells[threadIdx.x + 1][1][threadIdx.z + 1];

	float dMass = 0;
	dMass -= MyCell.flow.x.n + MyCell.flow.x.p;
	dMass -= MyCell.flow.y.n + MyCell.flow.y.p;
	dMass -= MyCell.flow.z.n + MyCell.flow.z.p;

	if (tid.x != 0) {
		dMass += localCells[threadIdx.x][1][threadIdx.z + 1].flow.x.p;
	}

	if (tid.x != 255) {
		dMass += localCells[threadIdx.x + 2][1][threadIdx.z + 1].flow.x.n;
	}

	if (tid.y != 0) {
		dMass += localCells[threadIdx.x + 1][0][threadIdx.z + 1].flow.y.p;
	}

	if (tid.y != 255) {
		dMass += localCells[threadIdx.x + 1][2][threadIdx.z + 1].flow.y.n;
	}

	if (tid.z != 0) {
		dMass += localCells[threadIdx.x + 1][1][threadIdx.z].flow.z.p;
	}

	if (tid.z != 255) {
		dMass += localCells[threadIdx.x + 1][1][threadIdx.z + 2].flow.z.n;
	}

	MyCell.mass += dMass;

	NextCells[tid.i] = MyCell;
}

