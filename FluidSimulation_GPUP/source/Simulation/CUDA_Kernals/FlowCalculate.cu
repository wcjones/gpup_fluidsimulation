#include "CUDA_Kernals.h"

using namespace std;

__global__ void FlowCalculation(CUDA_FluidCell *PreviousCells, CUDA_FluidCell *NextCells, const float delta, const float scale)
{
	ThreadIndex tid;

	__shared__ CUDA_FluidCell localCells[18][3][18];


	localCells[threadIdx.x + 1][1][threadIdx.z + 1] = PreviousCells[tid.i];


	//Load the data.
	if (blockIdx.y != 0 && blockIdx.y != 255) {
		//Load Current Position
		localCells[threadIdx.x + 1][0][threadIdx.z + 1] = PreviousCells[tid.i - 256];
		localCells[threadIdx.x + 1][2][threadIdx.z + 1] = PreviousCells[tid.i + 256];

		//X Axis
		if (threadIdx.x == 0) {
			if (blockIdx.x != 0 && blockIdx.x != 15) {
				localCells[0][1][threadIdx.z + 1] = PreviousCells[tid.i - 1];
				localCells[0][0][threadIdx.z + 1] = PreviousCells[tid.i - 256 - 1];
				localCells[0][2][threadIdx.z + 1] = PreviousCells[tid.i + 256 - 1];

				localCells[17][1][threadIdx.z + 1] = PreviousCells[tid.i + 16];
				localCells[17][0][threadIdx.z + 1] = PreviousCells[tid.i - 256 + 16];
				localCells[17][2][threadIdx.z + 1] = PreviousCells[tid.i + 256 + 16];

			}
			else if (blockIdx.x == 0) {
				localCells[17][1][threadIdx.z + 1] = PreviousCells[tid.i + 16];
				localCells[17][0][threadIdx.z + 1] = PreviousCells[tid.i - 256 + 16];
				localCells[17][2][threadIdx.z + 1] = PreviousCells[tid.i + 256 + 16];
			}
			else if (blockIdx.x == 15) {
				localCells[0][1][threadIdx.z + 1] = PreviousCells[tid.i - 1];
				localCells[0][0][threadIdx.z + 1] = PreviousCells[tid.i - 256 - 1];
				localCells[0][2][threadIdx.z + 1] = PreviousCells[tid.i + 256 - 1];
			}
		}

		//Z Axis
		if (threadIdx.z == 0) {
			if (blockIdx.z != 0 && blockIdx.z != 15) {
				localCells[threadIdx.x + 1][1][0] = PreviousCells[tid.i - 65536];
				localCells[threadIdx.x + 1][0][0] = PreviousCells[tid.i - 256 - 65536];
				localCells[threadIdx.x + 1][2][0] = PreviousCells[tid.i + 256 - 65536];

				localCells[threadIdx.x + 1][1][17] = PreviousCells[tid.i + 65536 * 16];
				localCells[threadIdx.x + 1][0][17] = PreviousCells[tid.i - 256 + 65536 * 16];
				localCells[threadIdx.x + 1][2][17] = PreviousCells[tid.i + 256 + 65536 * 16];
			}
			else if (blockIdx.z == 0) {
				localCells[threadIdx.x + 1][1][17] = PreviousCells[tid.i + 65536 * 16];
				localCells[threadIdx.x + 1][0][17] = PreviousCells[tid.i - 256 + 65536 * 16];
				localCells[threadIdx.x + 1][2][17] = PreviousCells[tid.i + 256 + 65536 * 16];
			}
			else if (blockIdx.z == 15) {
				localCells[threadIdx.x + 1][1][0] = PreviousCells[tid.i - 65536];
				localCells[threadIdx.x + 1][0][0] = PreviousCells[tid.i - 256 - 65536];
				localCells[threadIdx.x + 1][2][0] = PreviousCells[tid.i + 256 - 65536];
			}
		}

	}
	else if (blockIdx.y == 0) {
		//Load Current Position
		localCells[threadIdx.x + 1][2][threadIdx.z + 1] = PreviousCells[tid.i + 256];

		//X Axis
		if (threadIdx.x == 0) {
			if (blockIdx.x != 0 && blockIdx.x != 15) {
				localCells[0][1][threadIdx.z + 1] = PreviousCells[tid.i - 1];
				localCells[0][2][threadIdx.z + 1] = PreviousCells[tid.i + 256 - 1];

				localCells[17][1][threadIdx.z + 1] = PreviousCells[tid.i + 16];
				localCells[17][2][threadIdx.z + 1] = PreviousCells[tid.i + 256 + 16];

			}
			else if (blockIdx.x == 0) {
				localCells[17][1][threadIdx.z + 1] = PreviousCells[tid.i + 16];
				localCells[17][2][threadIdx.z + 1] = PreviousCells[tid.i + 256 + 16];
			}
			else if (blockIdx.x == 15) {
				localCells[0][1][threadIdx.z + 1] = PreviousCells[tid.i - 1];
				localCells[0][2][threadIdx.z + 1] = PreviousCells[tid.i + 256 - 1];
			}
		}

		//Z Axis
		if (threadIdx.z == 0) {
			if (blockIdx.z != 0 && blockIdx.z != 15) {
				localCells[threadIdx.x + 1][1][0] = PreviousCells[tid.i - 65536];
				localCells[threadIdx.x + 1][2][0] = PreviousCells[tid.i + 256 - 65536];

				localCells[threadIdx.x + 1][1][17] = PreviousCells[tid.i + 65536 * 16];
				localCells[threadIdx.x + 1][2][17] = PreviousCells[tid.i + 256 + 65536 * 16];
			}
			else if (blockIdx.z == 0) {
				localCells[threadIdx.x + 1][1][17] = PreviousCells[tid.i + 65536 * 16];
				localCells[threadIdx.x + 1][2][17] = PreviousCells[tid.i + 256 + 65536 * 16];
			}
			else if (blockIdx.z == 15) {
				localCells[threadIdx.x + 1][1][0] = PreviousCells[tid.i - 65536];
				localCells[threadIdx.x + 1][2][0] = PreviousCells[tid.i + 256 - 65536];
			}
		}
	}
	else if (blockIdx.y == 255) {
		//Load Current Position
		localCells[threadIdx.x + 1][0][threadIdx.z + 1] = PreviousCells[tid.i - 256];

		//X Axis
		if (threadIdx.x == 0) {
			if (blockIdx.x != 0 && blockIdx.x != 15) {
				localCells[0][1][threadIdx.z + 1] = PreviousCells[tid.i - 1];
				localCells[0][0][threadIdx.z + 1] = PreviousCells[tid.i - 256 - 1];

				localCells[17][1][threadIdx.z + 1] = PreviousCells[tid.i + 16];
				localCells[17][0][threadIdx.z + 1] = PreviousCells[tid.i - 256 + 16];

			}
			else if (blockIdx.x == 0) {
				localCells[17][1][threadIdx.z + 1] = PreviousCells[tid.i + 16];
				localCells[17][0][threadIdx.z + 1] = PreviousCells[tid.i - 256 + 16];
			}
			else if (blockIdx.x == 15) {
				localCells[0][1][threadIdx.z + 1] = PreviousCells[tid.i - 1];
				localCells[0][0][threadIdx.z + 1] = PreviousCells[tid.i - 256 - 1];
			}
		}

		//Z Axis
		if (threadIdx.z == 0) {
			if (blockIdx.z != 0 && blockIdx.z != 15) {
				localCells[threadIdx.x + 1][1][0] = PreviousCells[tid.i - 65536];
				localCells[threadIdx.x + 1][0][0] = PreviousCells[tid.i - 256 - 65536];

				localCells[threadIdx.x + 1][1][17] = PreviousCells[tid.i + 65536 * 16];
				localCells[threadIdx.x + 1][0][17] = PreviousCells[tid.i - 256 + 65536 * 16];
			}
			else if (blockIdx.z == 0) {
				localCells[threadIdx.x + 1][1][17] = PreviousCells[tid.i + 65536 * 16];
				localCells[threadIdx.x + 1][0][17] = PreviousCells[tid.i - 256 + 65536 * 16];
			}
			else if (blockIdx.z == 15) {
				localCells[threadIdx.x + 1][1][0] = PreviousCells[tid.i - 65536];
				localCells[threadIdx.x + 1][0][0] = PreviousCells[tid.i - 256 - 65536];
			}
		}
	}

	__syncthreads();

	CUDA_FluidCell MyCell = localCells[threadIdx.x + 1][1][threadIdx.z + 1];

	//Process Flow

	short stepMass = MyCell.mass;
	MyCell.flow.x.p = 0;
	MyCell.flow.x.n = 0;
	MyCell.flow.y.p = 0;
	MyCell.flow.y.n = 0;
	MyCell.flow.z.p = 0;
	MyCell.flow.z.n = 0;

	bool ProcessHorizontal = false;

	//Vertical Flow
	if (blockIdx.y != 0) {
		CUDA_FluidCell &DownCell = localCells[threadIdx.x + 1][0][threadIdx.z + 1];
		if (DownCell.mass < 100 && DownCell.type >= 0 && MyCell.mass != 0) {
			if ((100 - DownCell.mass) < MyCell.mass) {
				MyCell.flow.y.n += (100 - DownCell.mass);
			}
			else {
				MyCell.flow.y.n += MyCell.mass;
			}
		}
		else {
			ProcessHorizontal = true;
		}
	}

	stepMass += MyCell.flow.y.n;
	
	/*
	if (blockIdx.y != 255) {
		CUDA_FluidCell &UpCell = localCells[threadIdx.x + 1][2][threadIdx.z + 1];
		
		if (MyCell.mass < 100 && MyCell.type >= 0 && UpCell.mass != 0) {
			if ((100 - MyCell.mass) < UpCell.mass) {
				MyCell.flow.y.p += (100 - MyCell.mass);
			}
			else {
				MyCell.flow.y.p += UpCell.mass;
			}
		}
		else {
			MyCell.flow.y.p = 0;
		}
	}

	stepMass += MyCell.flow.y.p;
	*/

	//Horizontal Flow
	if (stepMass != 0 && ProcessHorizontal) {

		short tAvg = stepMass, nSum = 0;
		unsigned char minMass = stepMass, tNum = 1, nNum = 0;

		if (tid.x != 0 && !(localCells[threadIdx.x][1][threadIdx.z + 1].type == -1)) {
			tAvg += localCells[threadIdx.x][1][threadIdx.z + 1].mass;
			tNum++;

			if (localCells[threadIdx.x][1][threadIdx.z + 1].mass < stepMass) {
				nSum += localCells[threadIdx.x][1][threadIdx.z + 1].mass;
				nNum++;
			}

			if (localCells[threadIdx.x][1][threadIdx.z + 1].mass < minMass) {
				minMass = localCells[threadIdx.x][1][threadIdx.z + 1].mass;
			}

		}

		if (tid.x != 255 && !(localCells[threadIdx.x + 2][1][threadIdx.z + 1].type == -1)) {
			tAvg += localCells[threadIdx.x + 2][1][threadIdx.z + 1].mass;
			tNum++;

			if (localCells[threadIdx.x + 2][1][threadIdx.z + 1].mass < stepMass) {
				nSum += localCells[threadIdx.x + 2][1][threadIdx.z + 1].mass;
				nNum++;
			}

			if (localCells[threadIdx.x + 2][1][threadIdx.z + 1].mass < minMass) {
				minMass = localCells[threadIdx.x + 2][1][threadIdx.z + 1].mass;
			}

		}

		if (tid.z != 0 && !(localCells[threadIdx.x + 1][1][threadIdx.z].type == -1)) {
			tAvg += localCells[threadIdx.x + 1][1][threadIdx.z].mass;
			tNum++;

			if (localCells[threadIdx.x + 1][1][threadIdx.z].mass < stepMass) {
				nSum += localCells[threadIdx.x + 1][1][threadIdx.z].mass;
				nNum++;
			}

			if (localCells[threadIdx.x + 1][1][threadIdx.z].mass < minMass) {
				minMass = localCells[threadIdx.x + 1][1][threadIdx.z].mass;
			}

		}

		if (tid.z != 255 && !(localCells[threadIdx.x + 1][1][threadIdx.z + 2].type == -1)) {
			tAvg += localCells[threadIdx.x + 1][1][threadIdx.z + 2].mass;
			tNum++;

			if (localCells[threadIdx.x + 1][1][threadIdx.z + 2].mass < stepMass) {
				nSum += localCells[threadIdx.x + 1][1][threadIdx.z + 2].mass;
				nNum++;
			}

			if (localCells[threadIdx.x + 1][1][threadIdx.z + 2].mass < minMass) {
				minMass = localCells[threadIdx.x + 1][1][threadIdx.z + 2].mass;
			}

		}

		tAvg /= tNum;
		nSum -= stepMass * nNum;
		nSum *= -1;

		
		
		if (stepMass > minMass) {

			float dMass = 0;

			if (tid.x != 0 && !(localCells[threadIdx.x][1][threadIdx.z + 1].type == -1)) {
				dMass = localCells[threadIdx.x][1][threadIdx.z + 1].mass;
				if (dMass < stepMass) {
					dMass = (stepMass + dMass) * (stepMass - dMass) / (2 * nSum);
					MyCell.flow.x.n += dMass;
				}

			}



			if (tid.x != 255 && !(localCells[threadIdx.x + 2][1][threadIdx.z + 1].type == -1)) {
				dMass = localCells[threadIdx.x + 2][1][threadIdx.z + 1].mass;
				if (dMass < stepMass) {
					dMass = (stepMass + dMass) * (stepMass - dMass) / (2 * nSum);

					MyCell.flow.x.p += dMass;
				}

			}

			if (tid.z != 0 && !(localCells[threadIdx.x + 1][1][threadIdx.z].type == -1)) {
				dMass = localCells[threadIdx.x + 1][1][threadIdx.z].mass;
				if (dMass < stepMass) {
					dMass = (stepMass + dMass) * (stepMass - dMass) / (2 * nSum);

					MyCell.flow.z.n += dMass;
				}

			}



			if (tid.z != 255 && !(localCells[threadIdx.z + 1][1][threadIdx.z + 2].type == -1)) {
				dMass = localCells[threadIdx.z + 1][1][threadIdx.z + 2].mass;
				if (dMass < stepMass) {
					dMass = (stepMass + dMass) * (stepMass - dMass) / (2 * nSum);

					MyCell.flow.z.p += dMass;
				}

			}
			
		}

	}
	
	//Export the data.
	NextCells[tid.i] = MyCell;
	return;
	
}