#include "CUDA_Kernals.h"

using namespace std;

__global__ void CellRenderDataPush(CUDA_FluidCell* cells, glm::vec4* data)
{
	ThreadIndex tid;

	CUDA_FluidCell myCell = cells[tid.i];
	glm::vec4 myData = data[tid.i];

	myCell.mass += 1;
	
	if (myCell.type != 0) {
		if (myCell.type == -1)
			myData.w = -3;
		else
			myData.w = -myCell.type;
	}
	else {
		if (myCell.mass < 5) {
			myData.w = 0;
		}
		else {
			myData.w = (float)myCell.mass / 100.0f;
		}
	}

	myData.x = (fabsf(myCell.flow.x.p) + fabsf(myCell.flow.x.n)) / 250.0f;
	myData.y = (fabsf(myCell.flow.y.p) + fabsf(myCell.flow.y.n)) / 250.0f;
	myData.z = (fabsf(myCell.flow.z.p) + fabsf(myCell.flow.z.n)) / 250.0f;

	data[tid.i] = myData;
}
