#pragma once

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "Simulation.h"

struct cvec2 { char p, n; };

struct dvec3 { cvec2 x, y, z; };

struct cvec4 { unsigned char x, y, z, w; };

struct CUDA_FluidCell {
	/*
	-1 = Wall
	 0 = Normal
	 1 = Source
	 2 = Sink;
	*/
	char type;
	unsigned char mass;
	dvec3 flow;
	//short mass;
	//short capacity;

	__device__ CUDA_FluidCell() { };

	CUDA_FluidCell(FluidCell C) {
		flow.x.p = 0;
		flow.x.n = 0;
		flow.y.p = 0;
		flow.y.n = 0;
		flow.z.p = 0;
		flow.z.n = 0;

		mass = C.mass * 100;

		if (C.type == -3)
			type = -1;
		else
			type = -(char)C.type;
	};
};

struct ThreadIndex {
	unsigned char x, y, z;
	unsigned int i;

	__device__ ThreadIndex() {
		x = threadIdx.x + blockIdx.x * blockDim.x;
		y = threadIdx.y + blockIdx.y * blockDim.y;
		z = threadIdx.z + blockIdx.z * blockDim.z;
		i = x + y * 256 + z * 65536;
	}
};

//Calculation
__global__ void FlowCalculation(CUDA_FluidCell *PreviousCells, CUDA_FluidCell *NextCells, const float delta, const float scale);

//Update
__global__ void FlowUpdate(CUDA_FluidCell *PreviousCells, CUDA_FluidCell *NextCells);

//Post Step
__global__ void CellRenderDataPush(CUDA_FluidCell* cells, glm::vec4* data);