#include "Graphics.h"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#include <math.h>
#include <iostream>

static bool once = false;

int Graphics::renderCam() {

	// renderCam tests the camera only. There is only a need to send the data once.
	if (!once) {
		data->dataAttribute = glGetAttribLocation(data->renderProgram, "vertexData");
		glBindVertexArray(data->renderVAO);
		glBindBuffer(GL_ARRAY_BUFFER, data->vertexDataPointer);
		glBufferData(GL_ARRAY_BUFFER, 16 * (data->n_count), data->clientVertexData, GL_DYNAMIC_DRAW);
		once = true;
	}

	// Bind the current shader program to the OGL context
	glUseProgram(data->renderProgram);

	// Tightly packed attribute array
	glVertexAttribPointer(data->dataAttribute, 4, GL_FLOAT, GL_FALSE, 0, 0);

	// Enable the use of the vertexData attributes and use the currently
	//	bound VAO to do the operation.
	glEnableVertexAttribArray(data->dataAttribute);

	// View transform construction
	// ---

	glViewport(0, 0, data->window_size_x, data->window_size_y);

	// Window resize corrections
	const float aspect0 = (float)data->window_size_x / (float)data->window_size_y;
	const bool xGTy = data->window_size_x >= data->window_size_y;
	const float aspect = xGTy ? aspect0 : (1.0f/aspect0);
	const float fov = (3.14159f / 4.0f) * aspect;
	glm::mat4 CameraT = glm::perspective(fov, aspect0, 0.1f, 10000.0f);

	// Dear OGL, here is my ViewTransformMatrix
	data->uniCameraTransform = glGetUniformLocation(data->renderProgram, "CameraTransform");
	glUniformMatrix4fv(data->uniCameraTransform, 1, GL_FALSE, glm::value_ptr(CameraT));


	// Model transform construction
	// ---

	// Spherical coordinates produce a normal pointing to the camera location
	const float conversionFactor = 3.141590f / 180.0f;
	const float correctedTheta = data->theta * conversionFactor;
	const float correctedPhi = data->phi * conversionFactor;
	const glm::vec3 cameraVector = glm::vec3(
		cos(correctedTheta)*cos(correctedPhi),
		-sin(correctedPhi),
		sin(correctedTheta)*cos(correctedPhi) );
	const glm::vec3 cameraNormal = glm::normalize(cameraVector);
	data->position = cameraNormal * (data->radius / ((float)log10(aspect) + 1));

	// The originVec points to the center of the world, which we always look at
	const glm::mat4 orientationMat = glm::lookAt(-(cameraNormal), data->originVec, data->upVec);

	// Object transform is a translation to the camera and then rotation to origin
	glm::mat4 ObjectT = glm::mat4(1.0f) * orientationMat * (glm::translate(data->position));

	// Dear OGL, here is my ModelTransformMatrix:
	data->uniModelTransform = glGetUniformLocation(data->renderProgram, "ModelTransform");
	glUniformMatrix4fv(data->uniModelTransform, 1, GL_FALSE, glm::value_ptr(ObjectT));

	return 0;
}
