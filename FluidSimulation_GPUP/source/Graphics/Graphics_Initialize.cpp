#include "Graphics.h"
#include "UserX.h"
#include <iostream>

using namespace std;

int Graphics::Initialize() {

	// Load the extensions to OGL 4.5
	// TODO: Consider replacing with libepoxy
	glewExperimental = GL_TRUE;
	glewInit();

	// Allocate space and a name for vertex data
	glGenVertexArrays(1, &(data->renderVAO));
	glGenBuffers(1, &(data->vertexDataPointer));

	// Initialize the correct shaders.
	loadShaders(true);

	return 0;
}
