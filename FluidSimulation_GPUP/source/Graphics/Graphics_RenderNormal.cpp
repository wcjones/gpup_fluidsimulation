#include "Graphics.h"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/transform.hpp"
#include "glm/gtc/type_ptr.hpp"

int Graphics::renderNormal() {

	//Load the data if necessary, this is set by the simulation.
	if (data->reloadData) {
		//Data Packing is now done in Simulation Land

		// Get the location of the data attribute in the shaders
		data->dataAttribute = glGetAttribLocation(data->renderProgram, "vertexData");

		// All passes use this verticle information
		glBindVertexArray(data->renderVAO);

		// Bind the client vertex data to vertex attributes
		glBindBuffer(GL_ARRAY_BUFFER, data->vertexDataPointer);

		// Send data to the GPU
		glBufferData(GL_ARRAY_BUFFER, 16 * (data->n_count), data->clientVertexData, GL_STREAM_DRAW);

		//Reset the bool
		data->reloadData = false;
	}

	// Bind the current shader program to the OGL context
	glUseProgram(data->renderProgram);

	// Tightly packed attribute array
	glVertexAttribPointer(data->dataAttribute, 4, GL_FLOAT, GL_FALSE, 0, 0);

	// Enable the use of the vertexData attributes and use the currently
	//	bound VAO to do the operation.
	glEnableVertexAttribArray(data->dataAttribute);

	// View transform construction
	// ---

	glViewport(0, 0, data->window_size_x, data->window_size_y);

	// Window resize corrections
	const float aspect0 = (float)data->window_size_x / (float)data->window_size_y;
	const bool xGTy = data->window_size_x >= data->window_size_y;
	const float aspect = xGTy ? aspect0 : (1.0f/aspect0);
	const float fov = (3.14159f / 4.0f) * aspect;
	glm::mat4 CameraT = glm::perspective(fov, aspect0, 0.1f, 10000.0f);

	// Dear OGL, here is my ViewTransformMatrix
	data->uniCameraTransform = glGetUniformLocation(data->renderProgram, "CameraTransform");
	glUniformMatrix4fv(data->uniCameraTransform, 1, GL_FALSE, glm::value_ptr(CameraT));


	// Model transform construction
	// ---

	// Spherical coordinates produce a normal pointing to the camera location
	const float conversionFactor = 3.141590f / 180.0f;
	const float correctedTheta = data->theta * conversionFactor;
	const float correctedPhi = data->phi * conversionFactor;
	const glm::vec3 cameraVector = glm::vec3(
		cos(correctedTheta)*cos(correctedPhi),
		-sin(correctedPhi),
		sin(correctedTheta)*cos(correctedPhi) );
	const glm::vec3 cameraNormal = glm::normalize(cameraVector);
	data->position = cameraNormal * (data->radius / ((float)log10(aspect) + 1));

	// The originVec points to the center of the world, which we always look at
	const glm::mat4 orientationMat = glm::lookAt(-(cameraNormal), data->originVec, data->upVec);

	// Object transform is a translation to the camera and then rotation to origin
	glm::mat4 ObjectT = glm::mat4(1.0f) * orientationMat * (glm::translate(data->position));

	// The camera position is used in the geo and frag shaders for lighting calculations.
	GLuint camera_pos_uniform = glGetUniformLocation(data->renderProgram, "camera_position");
	glUniform4f(camera_pos_uniform, (data->position).x, (data->position).y, (data->position).z, 1.0f);

	// Dear OGL, here is my ModelTransformMatrix:
	data->uniModelTransform = glGetUniformLocation(data->renderProgram, "ModelTransform");
	glUniformMatrix4fv(data->uniModelTransform, 1, GL_FALSE, glm::value_ptr(ObjectT));

	return 0;
}
