#include "Graphics.h"
#include <iostream>

using namespace std;

static const GLfloat one = 1.0f;

int Graphics::Render() {

	// Before we render, make sure the correct shader program is loaded.
	// 	This is inefficiently called each pass. In the future, UserX calling
	//  this on each pass type change due ot a key stroke would be better.
	// TODO: Bool true is a hack.
	loadShaders(data->reload_shaders);

	glClearColor(0.9f, 0.9f, 0.9f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearBufferfv(GL_DEPTH, 0, &one);

	// All passes render 3D geometry

	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	// Each pass is responsible for packing its data. This allows each pass
	// 	to make efficient transfers based on its specialization. These passes
	// 	are more appropriately titled "pack and transform functions"
	switch (data->renderPass) {
		case REND_NORMAL:
				if (renderNormal() != 0) return 1;
				break;
		case REND_DEBUG:
				if (renderDebug() != 0) return 1;
				break;
		case REND_CAM:
				if (renderCam() != 0) return 1;
				break;
		default:
				return 2;
	}

	// Execute a pass and display on screen
	glPointSize(3);
	glDrawArrays(GL_POINTS, 0, data->n_count);
	glfwSwapBuffers(data->window);

	// Cleanup
	data->reload_shaders = false;

	return 0;
}
