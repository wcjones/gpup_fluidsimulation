#include "Graphics.h"

#include <string>
#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>

/** Get the status code and log for a shader or shader program */
void statusCheck(const GLint& status, const GLuint& gli, const bool& shader) {
	if (status != GL_TRUE) {

		std::cout << __LINE__ << ": LoadShaders, Status Check: SAD" << std::endl;

		GLint info_log_length;
		if (!shader) {
			glGetProgramiv(gli, GL_INFO_LOG_LENGTH, &info_log_length);
		} else {
			glGetShaderiv(gli, GL_INFO_LOG_LENGTH, &info_log_length);
		}

        char* const info_log = (char*) alloca(info_log_length+1);

		if (!shader) {
			glGetProgramInfoLog(gli, info_log_length+1, NULL, info_log);
		} else {
			glGetShaderInfoLog(gli, info_log_length+1, NULL, info_log);
		}

        printf("Status Info log:\n%s\n", info_log);

	} else {
		std::cout << __LINE__ << ": LoadShaders, Status Check: OK" << std::endl;
	}
}

/** Check whether we need to load a new render program into the current
		OGL context. */
int Graphics::loadShaders(const bool first_pass) {

	if (data->lastRenderPass != data->renderPass || first_pass) {

		std::cout << "LOADING SHADERS" << std::endl;

		// Undo any previous glCreateProgram() call
		if (!first_pass) {
			glDeleteProgram(data->renderProgram);
			glDeleteShader(data->vertexShader);
			glDeleteShader(data->fragmentShader);
			glDeleteShader(data->geometryShader);
		}

		// Set last pass type to the current pass type
		data->lastRenderPass = data->renderPass;

		// Determine which directory to pull shaders from
		std::string dirName;
		switch (data->renderPass) {
			case REND_NORMAL:
					dirName = "REND_NORMAL";
					break;
			case REND_DEBUG:
					dirName = "REND_DEBUG";
					break;
			case REND_CAM:
					dirName = "REND_CAM";
					break;
		}

		// Create new shaders in OGL context
		data->vertexShader = glCreateShader(GL_VERTEX_SHADER);
		data->geometryShader = glCreateShader(GL_GEOMETRY_SHADER);
		data->fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

		// Get the correct relative path for each rendering pass
		std::string vertexPath("./shaders/" + dirName + "/VertexShader.glsl");
		std::string geometryPath("./shaders/" + dirName + "/GeometryShader.glsl");
		std::string fragmentPath("./shaders/" + dirName + "/FragmentShader.glsl");

		// Open an input stream to the source files
		std::ifstream vertexShaderSourceFile(vertexPath);
		std::ifstream geometryShaderSourceFile(geometryPath);
		std::ifstream fragmentShaderSourceFile(fragmentPath);

		// Get the source contents to const char* via the input streams
		std::string vertexShaderSourceStr((std::istreambuf_iterator<char>(vertexShaderSourceFile)), std::istreambuf_iterator<char>());
		std::string geometryShaderSourceStr((std::istreambuf_iterator<char>(geometryShaderSourceFile)), std::istreambuf_iterator<char>());
		std::string fragementShaderSourceStr((std::istreambuf_iterator<char>(fragmentShaderSourceFile)), std::istreambuf_iterator<char>());

		const char* vertexShaderSource = vertexShaderSourceStr.c_str();
		const char* geometryShaderSource = geometryShaderSourceStr.c_str();
		const char* fragementShaderSource = fragementShaderSourceStr.c_str();

		// Upload the source contents for each shader
		glShaderSource(data->vertexShader, 1, &vertexShaderSource, NULL);
		glShaderSource(data->geometryShader, 1, &geometryShaderSource, NULL);
		glShaderSource(data->fragmentShader, 1, &fragementShaderSource, NULL);


		// Shader compilation
		// ---

		GLint status;

		glCompileShader(data->vertexShader);
		glGetShaderiv(data->vertexShader, GL_COMPILE_STATUS, &status);
		statusCheck(status, data->vertexShader, true);

		glCompileShader(data->geometryShader);
		glGetShaderiv(data->geometryShader, GL_COMPILE_STATUS, &status);
		statusCheck(status, data->geometryShader, true);

		glCompileShader(data->fragmentShader);
		glGetShaderiv(data->fragmentShader, GL_COMPILE_STATUS, &status);
		statusCheck(status, data->fragmentShader, true);

		// Shader linking, program creation, and binding.
		// ---

		data->renderProgram = glCreateProgram();

		glAttachShader(data->renderProgram, data->vertexShader);
		glAttachShader(data->renderProgram, data->geometryShader);
		glAttachShader(data->renderProgram, data->fragmentShader);

		glBindFragDataLocation(data->renderProgram, 0, "outColor");

		glLinkProgram(data->renderProgram);
		glGetProgramiv(data->renderProgram, GL_LINK_STATUS, &status);
		statusCheck(status, data->renderProgram, false);

	}

	return 0;
}
