#include "UserX.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "GLFW/glfw3.h"

#include <cmath>
#include <iostream>

using namespace std;

int UserX::Update() {

	glfwGetWindowSize(data->window, &(data->window_size_x), &(data->window_size_y));

	// Distance from world cube:

	float radialMovement = 0;

	if (glfwGetKey(data->window, GLFW_KEY_W)) {
		radialMovement -= data->move_speed * data->time.deltaUXTime;
	}
	if (glfwGetKey(data->window, GLFW_KEY_S)) {
		radialMovement += data->move_speed * data->time.deltaUXTime;
	}

	// TODO: #Define radius minimum and default.
	data->radius += radialMovement;
	if (data->radius < 32.0f) data->radius = 32.0f;


	// Position about the world cube:

	if (glfwGetKey(data->window, GLFW_KEY_I)) {
		data->phi += data->angles_per_cycle * data->time.deltaUXTime;
	}
	if (glfwGetKey(data->window, GLFW_KEY_K)) {
		data->phi -= data->angles_per_cycle * data->time.deltaUXTime;
	}

	if (glfwGetKey(data->window, GLFW_KEY_L)) {
		data->theta -= data->angles_per_cycle * data->time.deltaUXTime;
	}
	if (glfwGetKey(data->window, GLFW_KEY_J)) {
		data->theta += data->angles_per_cycle * data->time.deltaUXTime;
	}

	if (data->theta >= 360.0f || data->theta < 0.0f) data->theta = fmod(data->theta, 360.0f);
	if (data->phi > 90.0f) data->phi = 90.0f;
	if (data->phi < -90.0f) data->phi = -90.0f;


	// Additional functionality:

	// Reset (R):
	if (glfwGetKey(data->window, GLFW_KEY_R)) {
		data->radius = 1024.0f;		// TODO: reset to #DEFINE'd
		data->theta = 180.0f;
		data->phi = 0.0f;
	}

	// Camera Mode (C)
	static bool last_get_key_set = false;
	bool get_key_set = glfwGetKey(data->window, GLFW_KEY_C) == GLFW_PRESS;

	if (get_key_set && last_get_key_set) return 0;

	if (get_key_set && !last_get_key_set) {
		last_get_key_set = true;
		if (data->renderPass == REND_NORMAL) {
			data->renderPass = REND_DEBUG;
			std::cout << "SET TO DEBUG RENDER PATH" << std::endl;
		} else if(data->renderPass == REND_DEBUG) {
			data->renderPass = REND_NORMAL;
			std::cout << "SET TO NORMAL RENDER PATH" << std::endl;
		} else {
			data->renderPass = REND_NORMAL;
			std::cout << "SET TO NORMAL RENDER PATH" << std::endl;
		}
		return 0;
	}

	if (!get_key_set && last_get_key_set)
		last_get_key_set = false;

	// Reload Shaders (Z):
	if (glfwGetKey(data->window, GLFW_KEY_Z)) {
		data->reload_shaders = true;
	}


	return 0;
}
