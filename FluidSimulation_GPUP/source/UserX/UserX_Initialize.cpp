#include "UserX.h"

int UserX::Initialize() {

	// Note: This must be initialized before GLEW
	// TODO: Consider libepoxy in place of GLEW, requires no init.s
	glfwInit();

	// Create the window and context:
	static const char* title = "Fluid Simulation - William Jones & Seth Junot";
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
	data->window = glfwCreateWindow(WINDOW_SIZE_X, WINDOW_SIZE_Y, title, 0, 0);
	glfwMakeContextCurrent(data->window);

	return 0;
}
