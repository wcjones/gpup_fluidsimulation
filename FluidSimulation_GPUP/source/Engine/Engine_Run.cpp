#include "Engine.h"

int Engine::Run() {
	while (data->applicationStatus == NORMAL) {
		glfwPollEvents();
		if (glfwWindowShouldClose(data->window)) data->applicationStatus = CLOSE;
		data->time.updateTime();
		if (data->time.tryStepUXTime()) userx->Update();
		if (data->time.tryStepCalcTime()) simulation->Step();
		if (data->time.tryStepRenderTime()) graphics->Render();
	}
	return 0;
}
