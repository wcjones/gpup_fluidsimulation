#include "Engine.h"

using namespace std;

Engine::Engine() {
	data = new Data();

	simulation = new Simulation(data);
	graphics = new Graphics(data);
	userx = new UserX(data);
}
