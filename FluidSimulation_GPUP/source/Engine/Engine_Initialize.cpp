#include "Engine.h"

int Engine::Initialize() {

	userx->Initialize();
	simulation->Initialize();
	graphics->Initialize();

	return 0;
}
