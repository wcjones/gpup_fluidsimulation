#
# GPUP-Proj Linux Makefile
# 	Build output is placed in linux_bld/*
#

# A few assumptions I make:
# 	You have GNU ld, make, X11, etc..

INCDIR = include/
LIBDIR = lib/linux_lib64/

NVCC = nvcc
NVCCFLAGS = -std c++11
CUDART = /usr/local/cuda/lib64/

CXX = clang++
CXXFLAGS = -std=c++11 -Wall -Wpedantic -fcolor-diagnostics -fansi-escape-codes -O3
LDLIBS = -lm -latomic -lGL -lGLEW -lglfw3 -lX11 -lXrandr -lXinerama -lXi -lXxf86vm -lXcursor -lpthread -ldl -lcudart

CPPSOURCES = $(shell find -L source/ -type f -iname "*.cpp")
CPPOBJECTS = $(CPPSOURCES:%.cpp=linux_bld/out/cpp/%.o)

NVCSOURCES = $(shell find -L source/ -type f -iname "*.cu")
NVCOBJECTS = $(NVCSOURCES:%.cu=linux_bld/out/nvcc/%.o)

PROGRAM = linux_bld/bin/gpup_fluid_sim

all: init | $(CPPOBJECTS) $(NVCOBJECTS) $(PROGRAM)

$(PROGRAM): $(CPPOBJECTS) $(NVCOBJECTS)
	$(CXX) $(CXXFLAGS) -L$(LIBDIR) -L$(CUDART) -o $@ $(CPPOBJECTS) $(NVCOBJECTS) $(LDLIBS)

linux_bld/out/cpp/%.o: %.cpp
	mkdir -p linux_bld/out/cpp/$(shell dirname $<)
	$(CXX) $(CXXFLAGS) -I$(INCDIR) -c $< -o $@

linux_bld/out/nvcc/%.o: %.cu
	mkdir -p linux_bld/out/nvcc/$(shell dirname $<)
	$(NVCC) $(NVCCFLAGS) -I$(INCDIR) -I/usr/local/cuda/include/ -c $< -o $@

init:
	mkdir -p linux_bld/out/cpp/
	mkdir -p linux_bld/out/nvcc/
	mkdir -p linux_bld/bin/

clean:
	rm -rf linux_bld/ $(PROGRAM)
